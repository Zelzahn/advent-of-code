import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;

class AoC {
    static int fst_puzzle(String filename) {
        BufferedReader reader;
        int total = 0;

        try {
            reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();

            while (line != null) {
                String[] parts = line.split(":");
                String[] numbers = parts[1].trim().split("\\|");
                int[] winning_numbers = Arrays.stream(numbers[0].split(" "))
                        .filter(Predicate.not(String::isEmpty))
                        .mapToInt(Integer::parseInt).toArray();

                Supplier<IntStream> our_numbers = () -> Arrays.stream(numbers[1].split(" "))
                        .filter(Predicate.not(String::isEmpty))
                        .mapToInt(Integer::parseInt);

                int subtotal = 0;
                for (int winner : winning_numbers) {
                    if (our_numbers.get().anyMatch(x -> x == winner)) {
                        if (subtotal == 0) {
                            subtotal = 1;
                        } else {
                            subtotal *= 2;
                        }
                    }
                }

                total += subtotal;
                // read next line
                line = reader.readLine();
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return total;
    }

    static int snd_puzzle(String filename) {
        BufferedReader reader;
        HashMap<Integer, Integer> cardCopiesMap = new HashMap<Integer, Integer>();

        try {
            reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            int currentCard = 1;

            while (line != null) {
                cardCopiesMap.putIfAbsent(currentCard, 1);
                String[] parts = line.split(":");
                String[] numbers = parts[1].trim().split("\\|");
                int[] winningNumbers = Arrays.stream(numbers[0].split(" "))
                        .filter(Predicate.not(String::isEmpty))
                        .mapToInt(Integer::parseInt).toArray();

                Supplier<IntStream> ourNumbers = () -> Arrays.stream(numbers[1].split(" "))
                        .filter(Predicate.not(String::isEmpty))
                        .mapToInt(Integer::parseInt);

                // for (Map.Entry<Integer, Integer> entry : cardCopiesMap.entrySet()) {
                // System.out.println(entry.getKey() + " : " + entry.getValue());
                // }
                // System.out.println("---");
                int currentCardCopyIndex = currentCard + 1;
                for (int winner : winningNumbers) {
                    if (ourNumbers.get().anyMatch(x -> x == winner)) {
                        cardCopiesMap.put(currentCardCopyIndex,
                                cardCopiesMap.getOrDefault(currentCardCopyIndex, 1)
                                        + cardCopiesMap.get(currentCard));

                        currentCardCopyIndex++;
                    }
                }

                // read next line
                line = reader.readLine();
                currentCard++;
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cardCopiesMap.values().stream().reduce(0, (subtotal, element) -> subtotal + element);
    }

    public static void main(String[] args) {
        // System.out.println(snd_puzzle("./input/subset"));
        System.out.println(snd_puzzle("./input/day_4"));
    }
}