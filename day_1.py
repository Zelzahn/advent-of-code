import re

"""
sum = 0

for line in file:
    find the 2 numbers in the line (in order)
    sum += int(string(the 2 numbers))

return sum
"""
def fst_puzzle(filename):
    sum = 0

    with open(filename) as file:
        for line in file:
            matches = re.findall(r'\d', line)
            sum += int("".join([matches[0], matches[-1]]))

    return sum

print(f"Solution of the first puzzle: {fst_puzzle('input/day_1')}")

def snd_puzzle(filename):
    def to_int(string):
       try:
        return int(string)
       except ValueError:
        return int(["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"].index(string)) + 1

    sum = 0

    with open(filename) as file:
        for line in file:
            matches = re.findall(r'(?=(\d|one|two|three|four|five|six|seven|eight|nine))', line)
            sum += int(f"{to_int(matches[0])}{to_int(matches[-1])}")

    return sum

print(f"Solution of the second puzzle: {snd_puzzle('input/day_1')}")