#!/bin/sh

fst_puzzle () {
    n=()
    s=()
    i=0
    while read -r line
    do
        j=0
        k=-1
        while read -n1 a
        do
            if [ "$a" != "." ] && [ "$a" != "" ]; then
                if echo "$a" | grep -q -v -E '^[0-9]$'; then
                    s+=("$i $j")
                else
                    n+=("$i $j $a")
                fi

                if [ $k -ne $j ]; then
                    for ki in $(seq $(($k + 1)) $j); do
                        echo "$ki : ${n[ki]}"
                        # ${foo/%from/to}
                        # s[$ki]=${s[$ki]/%([0-9]+)/\1$a}
                        ${s[$ki]/%([0-9]+)/\1$a}
                    done
                fi
            else
                k=$j
            fi
            
            ((++j))
        done <<< $line
        
        ((++i))
    done < $1

    echo "n"
    for i in "${n[@]}"
    do
    	echo "$i"
    done
    
    # echo "s"
    # res=0
    # for index in "${s[@]}"
    # do
    #     i=($(echo ${index##* }))
    #     j=($(echo ${index## *}))

    #     # echo "$i $j"
    #     for elem in ${n[@]}; do
    #         ei=($(echo ${elem##* }))
    #         ej=($(echo ${elem## * }))

    #         diff_i=($(echo $(($ei-$i)) | sed 's/-//')) 
    #         diff_j=($(echo $(($ej-$j)) | sed 's/-//')) 
            
    #         # if diff_i && diff_j == 1: val *= 
    #         if [ $diff_i -le 1 ] && [ $diff_j -le 1 ]; then
    #            val=($(echo ${elem## * * })) 
    #            ((res += val))
    #         fi
    #     done

    # done
    
    # printf "%s\n" "$res"
}

fst_puzzle ./input/subset
# fst_puzzle ./input/day_3