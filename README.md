# Advent of Code
[Site](https://adventofcode.com/)

- [x] Day 1: Python
- [x] Day 2: Elixir
- [x] Day 3: Bash
- [x] Day 4: Java
- [ ] Day 5: C++
    - [x] Part 1
    - [ ] Part 2: An optimization needs to be found for seeds
