defmodule Puzzle do
  defp check_color(amount, "blue"), do: amount < 15
  defp check_color(amount, "red"), do: amount < 13
  defp check_color(amount, "green"), do: amount < 14
  defp check_color(_, _), do: true

  def fst_puzzle(filename) do
    File.stream!(filename)
    |> Enum.reduce(0, fn line, acc ->
      [id | elems] =
        String.replace(line, ~r/;|:|,/, "")
        |> String.split()
        |> Enum.drop(1)

      valid? =
        Enum.chunk_every(elems, 2)
        |> Enum.all?(fn [amount, color] ->
          check_color(String.to_integer(amount), color)
        end)

      if valid? do
        acc + String.to_integer(id)
      else
        acc
      end
    end)
  end

  defp get_max_color(elems, color),
    do:
      Enum.filter(elems, fn [_, c] -> c == color end)
      |> Enum.map(fn [amnt, _] -> amnt end)
      |> Enum.max()

  def snd_puzzle(filename) do
    File.stream!(filename)
    |> Enum.reduce(0, fn line, acc ->
      [_ | elems] =
        String.replace(line, ~r/;|:|,/, "")
        |> String.split()
        |> Enum.drop(1)

      elems = Enum.chunk_every(elems, 2) |> Enum.map(fn [t, c] -> [String.to_integer(t), c] end)

      greens = get_max_color(elems, "green")
      reds = get_max_color(elems, "red")
      blues = get_max_color(elems, "blue")
      acc + greens * reds * blues
    end)
  end
end

IO.write("Solution of the first puzzle: ")
IO.inspect(Puzzle.fst_puzzle("./input/day_2"))
IO.write("Solution of the second puzzle: ")
IO.inspect(Puzzle.snd_puzzle("./input/day_2"))
