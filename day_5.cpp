#include <iostream>
#include <utility>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <algorithm>
#include <memory>
#include <inttypes.h>

class Seed { 
    public:
        double value; 
        Seed(double value): value(value) {}
};
typedef std::vector<std::shared_ptr<Seed>> Seeds;
typedef std::tuple<double, double, double> Range;
class Category {
    public:
        std::shared_ptr<Category> destination;

        Category(): destination(nullptr) {}
        double corresponding(double source);
        void add_range(double dest_start, double src_start, double length);
        void set_destination(std::shared_ptr<Category> dest) { this->destination = dest; }
    private:
        // std::map<double, double> mapping;
        std::vector<Range> mapping;
};

void Category::add_range(double dest_start, double src_start, double length) {
    mapping.push_back(Range(src_start, length, dest_start));
}

double Category::corresponding(double source) {
    double res = source;

    // Apparently ranges do not overlap
    for(Range range: this->mapping) {
        if(source >= std::get<0>(range) && source < std::get<0>(range) + std::get<1>(range))
            res = std::get<2>(range) + (source - std::get<0>(range));
    }

    return res;
}

std::pair<Seeds, std::shared_ptr<Category>> parse_file(std::string filename, bool is_snd_part) {
    std::ifstream file(filename);

    if (!file.is_open()) {
        std::cerr << "failed to open: " << filename << '\n';
        exit(-1);
    }

    std::string line;
    std::getline(file, line);

    Seeds seeds = {};
    std::shared_ptr<Category> from_category = std::make_shared<Category>();
    std::shared_ptr<Category> category = from_category;

    std::stringstream test(line);
    std::string segment;
    // The first segment is "seeds: "
    std::getline(test, segment, ' ');

    if(!is_snd_part) {
        while (std::getline(test, segment, ' ')) if (!segment.empty()) seeds.push_back(std::make_shared<Seed>(std::stod(segment)));
    } else {
        while (std::getline(test, segment, ' ') && !segment.empty()) {
            double start = std::stod(segment);
            
            std::getline(test, segment, ' ');
            double length = std::stod(segment);
            // std::cout << "start: " << start << " length: " << length << std::endl;
            for(double i = 0; i < length; ++i) {
                Seed* seed = new Seed(start + i);
                seeds.push_back(std::shared_ptr<Seed>(seed));
            }
            std::cout << "size of seeds: " << seeds.size() << std::endl;
        }
    }


    // Skip whiteline after seeds
    std::getline(file, line);

    // Skip header line
    while(std::getline(file, line)) {
        std::shared_ptr<Category> to_category = std::make_shared<Category>();
        // Process category
        while(std::getline(file, line) && line != "") {
            std::stringstream line_stream(line);
            double dest_start, src_start, length;
            line_stream >> dest_start >> src_start >> length;

            to_category->add_range(dest_start, src_start, length);
        }

        from_category->set_destination(to_category);
        from_category = to_category;
    }


    return std::make_pair(seeds, category);
}

double puzzle(std::string filename, bool is_snd_part) {
    auto [seeds, category] = parse_file(filename, is_snd_part);

    // First one is just an init
    category = category->destination;
    while(category != nullptr) {
        for(std::shared_ptr<Seed> seed: seeds)
            seed->value = category->corresponding(seed->value);

        category = category->destination;
    }

    // for(std::shared_ptr<Seed> seed: seeds) {
    //     std::cout << "Seed value: " << seed->value << std::endl;
    // }

    return (*std::min_element(seeds.begin(), seeds.end(), [](std::shared_ptr<Seed> a, std::shared_ptr<Seed> b) { return a->value < b->value; }))->value;
}

int main() {
    // std::cout << "Result of puzzle: " << puzzle("input/subset", true) << std::endl;
    std::cout << "Result of puzzle: " << puzzle("input/day_5", true) << std::endl;

    return 0;
}

