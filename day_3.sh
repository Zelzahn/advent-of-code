#!/bin/sh

fst_puzzle () {
    matches=()
    readarray -t input < $1
    i=0
    while read -r line; do
        j=0
        while read -n1 a; do
            # Check if special character
            if [ "$a" != "." ] && [ "$a" != "" ] && echo "$a" | grep -q -v -E '^[0-9]$'; then
                # echo "$a"
                possible_matches=()
                for r in $(seq 0 2); do
                    row_idx=$(echo $r-1+$i | bc)
                    row=$(echo "${input[$row_idx]}")
                    # echo "$row_idx"
                    c=0
                    while [ $c -lt 3 ]; do
                    # for c in $(seq 0 2); do
                        # echo "col: $c"
                        if [ $r -eq 1 ] && [ $c -eq 1 ]; then
                            ((++c))
                            continue
                        fi

                        col_idx=$(echo $c-1+$j | bc)
                        piece=$(echo "${row:$col_idx:1}")
                        
                        # L -> R
                        # if [ $c -eq 0 ]; then
                        if [ "$piece" != "." ]; then
                            # echo "$piece / $col_idx / $row"
                            piece_left=$(echo "${row:0:$col_idx}")
                            piece_left=${piece_left##*[^0-9]}

                            piece_right=$(echo "${row:$col_idx}")
                            piece_right=${piece_right%%[^0-9]*}

                            possible_matches+=("$piece_left$piece_right")

                            ((c+=${#piece_right}))
                        fi
                        
                        ((++c))
                    done
                done

                for k in ${possible_matches[@]}; do
                    # echo "$k"
                    matches+=($k)
                done
            fi
            
            ((++j))
        done <<< $line
        
        ((++i))
    done < $1
                
    # read -a possible_matches
    tot=0
    for i in ${matches[@]}; do
      let tot+=$i
    done
    echo "Total: $tot"
}

snd_puzzle () {
    matches=()
    readarray -t input < $1
    i=0
    while read -r line; do
        j=0
        while read -n1 a; do
            # Check if special character
            if [ "$a" == "*" ]; then
                # echo "$a"
                possible_matches=()
                for r in $(seq 0 2); do
                    row_idx=$(echo $r-1+$i | bc)
                    row=$(echo "${input[$row_idx]}")
                    # echo "$row_idx"
                    c=0
                    while [ $c -lt 3 ]; do
                    # for c in $(seq 0 2); do
                        # echo "col: $c"
                        if [ $r -eq 1 ] && [ $c -eq 1 ]; then
                            ((++c))
                            continue
                        fi

                        col_idx=$(echo $c-1+$j | bc)
                        piece=$(echo "${row:$col_idx:1}")
                        
                        # L -> R
                        # if [ $c -eq 0 ]; then
                        if [ "$piece" != "." ]; then
                            # echo "$piece / $col_idx / $row"
                            piece_left=$(echo "${row:0:$col_idx}")
                            piece_left=${piece_left##*[^0-9]}

                            piece_right=$(echo "${row:$col_idx}")
                            piece_right=${piece_right%%[^0-9]*}

                            possible_matches+=("$piece_left$piece_right")

                            ((c+=${#piece_right}))
                        fi
                        
                        ((++c))
                    done
                done

                if [ ${#possible_matches[@]} -eq 2 ]; then
                    tot=1
                    for k in ${possible_matches[@]}; do
                        let tot*=$k
                    done
                    matches+=($tot)
                fi
            fi
            
            ((++j))
        done <<< $line
        
        ((++i))
    done < $1
                
    # read -a possible_matches
    tot=0
    for i in ${matches[@]}; do
      let tot+=$i
    done
    echo "Total: $tot"
}

# snd_puzzle ./input/subset
snd_puzzle ./input/day_3
